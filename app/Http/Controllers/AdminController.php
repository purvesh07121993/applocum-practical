<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Routes;
use App\User;
use App\Roles;
use Auth;
use Input;
use Redirect;
use Mail;
use Hash;

class AdminController extends Controller{


    /**
     * @author Purvesh Patel
     * Created date: 
     */
	public function __construct(){
	    $this->middleware('admin');
	}

    /**
	 * Descripation: use to admin
	 * @author Purvesh Patel
	 * Created date: 19 Desc 2018 10:30 AM
	 */
    public function index(){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 

    	return view('admin.dashboard')->with(['accessData'=>$accessData]);
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function profileUpdate(){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 

    	return view('admin.profile')->with(['accessData'=>$accessData]);
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function profileUpdateSave(Request $request, $id){
    	$postData   = Input::all();

        $rules = [
            'full_name'     => 'required', //validate role input
            'email'         => 'required|email|unique:users,email,'.$id.',user_id',
        ];
        if($request->hasFile('photo') == true){
            $rules['photo'] = 'required|max:1024|mimes:jpeg,jpg,png';
        }
        
        $this->validate($request, $rules);
        
        //dd($postData);
        $fileName   = Input::get('photo_hidden');
        if($request->hasFile('photo') == true){
            $file       = $request->file('photo');
            $fileName   = strtotime(date("Y-m-d H:i:s")).$file->getClientOriginalName();
            $file->move('assets/uploads/user/', $fileName);
        }

        $name           = Input::get('full_name');
        $email          = Input::get('email');
        $mobile         = Input::get('mobile');

        User::where('user_id', $id)->update(array(
            'full_name'         =>  $name,
            'email'             =>  $email,
            'mobile'            =>  $mobile,
            'photo'             =>  $fileName,
            'updated_by'        =>  $id
        ));

        return redirect()->route('admin-profile')->with('success', 'Profile has been updated successfully.');
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function changePassword(){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 

        return view('admin.change_password')->with(['accessData'=>$accessData]);
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function changePasswordSave(request $request){
        $postData   = Input::all();
        $this->validate($request, [
            'current_password'  => 'required',
            'new_password'      => 'required|min:6',
            'confirm_password'  => 'required|same:new_password|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);

        $current_password = Auth::User()->password;           
        if(Hash::check(Input::get('current_password'), $current_password)){           
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt(Input::get('new_password'));
            $user->save();
            return redirect()->back()->with("success","Password has been changed successfully.");
        }
        else{
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function adminList(){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 
        $userData = User::whereIn('role_type', ["Companies"])->where('status','1')->orderBy('user_id', 'DESC')->get();
        //dd($userData->toArray());
        return view('admin.admin_list')->with(['userData' => $userData ,'accessData' => $accessData]);
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function adminAdd(){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 

        $roleData = Roles::whereIn('role_type', ["Companies"])->where('status','1')->get();
        //dd($userData->toArray());
        return view('admin.admin_add')->with(['roleData' => $roleData , 'accessData'=> $accessData]);
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function saveAdmin(request $request){
        $loginUser       = Auth::user();

        $rules = [
            'role'      => 'required|string',
            'full_name' => 'required|string|max:255',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ];
        if($request->hasFile('photo') == true){
            $rules['photo'] = 'required|max:1024|mimes:jpeg,jpg,png|dimensions:min_width=100,min_height=100';
        }
        
        $this->validate($request, $rules);

        $fileName   = "";
        if($request->hasFile('photo') == true){
            $file       = $request->file('photo');
            $fileName   = strtotime(date("Y-m-d H:i:s")).$file->getClientOriginalName();
            $file->move('assets/uploads/user/', $fileName);
        }
        //dd(Input::get());

        $roleTypeData = Roles::where('role_id',Input::get('role'))->get();
        
        $password = Input::get('password');
        $data = User::create([
            'user_number'   => strtotime(date("Y-m-d")),
            'fk_role_id'    => Input::get('role'),
            'full_name'     => Input::get('full_name'),
            'email'         => Input::get('email'),
            'password'      => bcrypt($password),
            'role_type'     => $roleTypeData[0]['role_type'],
            'mobile'        => Input::get('mobile'),
            'photo'         => $fileName,
            'status'            => 1,
            'is_new_user'       => 1,
            'created_by'        => $loginUser['user_id']
        ]);
        //dd($data->toArray());

        $insertId           = $data['user_id'];
        $User               = User::find($insertId);
        $User->user_number  = "100".$insertId;
        $User->save();

        $user = User::findOrFail($insertId);

        Mail::send('emails.welcome', ['user' => $user,'password' => $password], function ($m) use ($user) {
            //$m->from('developerhemshub@gmail.com', 'PHP');
            $m->to($user->email, $user->full_name)->subject('Welcome to Laravel');
        });

        return redirect()->route('admin-user')->with('success', 'Companies user has been added successfully.');
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function adminUpdate($id){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 

        $userData = User::findOrFail($id);
        $roleData = Roles::whereIn('role_type', ["Companies"])->where('status','1')->get();
        //dd($userData->toArray());
        return view('admin.admin_update')->with(['userData'=> $userData,'roleData'=>$roleData ,'accessData' => $accessData]);
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function saveUpdateAdmin(request $request, $id){
        $loginUser       = Auth::user();
        $rules = [
            'role'      => 'required|string',
            'full_name' => 'required|string|max:255',
            'email'     => 'required|email|unique:users,email,'.$id.',user_id',
        ];
        if($request->hasFile('photo') == true){
            $rules['photo'] = 'required|max:1024|mimes:jpeg,jpg,png|dimensions:min_width=100,min_height=100';
        }
        
        $this->validate($request, $rules);

        $fileName   = Input::get('photo_hidden');
        if($request->hasFile('photo') == true){
            $file       = $request->file('photo');
            $fileName   = strtotime(date("Y-m-d H:i:s")).$file->getClientOriginalName();
            $file->move('assets/uploads/user/', $fileName);
        }
        //dd(Input::get());

        $roleTypeData = Roles::where('role_id',Input::get('role'))->get();

        User::where('user_id', $id)->update(array(
            'fk_role_id'    => Input::get('role'),
            'full_name'     => Input::get('full_name'),
            'email'         => Input::get('email'),
            'role_type'     => $roleTypeData[0]['role_type'],
            'mobile'        => Input::get('mobile'),
            'photo'         => $fileName,
            'updated_by'    => $loginUser['user_id']
        ));

        return redirect()->route('admin-user')->with('success', 'Companies user has been updated successfully.');
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function deleteAdmin($id){
        //$accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin-user')->with('success', 'Companies user has been deleted successfully.');
    }


    /**
     * @author Purvesh Patel
     * Created date: 
     */
    public function adminDetailsView($id){
        $accessData    = $this->getArray('admin-user',Auth::user()->fk_role_id); 
        //$userData = User::whereIn('users.role_type', ["Admin"])->where(['users.status'=>'1','users.user_id'=>$id])->get();
        
        $userData = User::whereIn('users.role_type', ["Companies"])->where(['users.status'=>'1','users.user_id'=>$id])
                    ->select(
                        'users.*',
                        'roles.name as role_name'
                    )
                    ->leftJoin('roles', 'roles.role_id', '=', 'users.fk_role_id')
                    ->get();
        //dd($userData->toArray());
    return view('admin.admin_details')->with(['userData'=> $userData,'accessData'=>$accessData]);
    }
}
