<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Input;
use Validator;
use App\User;
use Redirect;
use Mail;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Descripation: use to login page design
     * @author Purvesh Patel
     * Created date: 18 Desc 2018 10:30 AM
     */
    public function showLoginForm(){
        return view('auth/login');
    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect()->route('login-design');
    }


    /**
     * Descripation: use to forget Password page design
     * @author Purvesh Patel
     * Created date: 18 Desc 2018 10:55 AM
     */
    public function forgetPassword(){
        return view('auth/forget_password');
    }


    /**
     * Descripation: use to admin user forget Password email Authentication
     * @author Purvesh Patel
     * Created date: 18 Desc 2018 11:55 AM
     */
    public function forgetPasswordAuthentication(Request $request){
        $postData = Input::all();
        $validatedData = Validator::make($request->all(), [
            'email' => 'required|email',
        ])->validate();

        $email1 = Input::get('email');

        $userData = User::where(['status'=>'1','email'=>$email1])->get();
        //dd($userData->toArray());

        if(is_array($userData->toArray()) && count($userData->toArray()) > 0){

            Mail::send('emails.reset_password', ['user' => "test",'password' => "1234567"], function ($m) use ($userData) {
                //$m->from('developerhemshub@gmail.com', 'PHP');
                $m->to($userData[0]->email, $userData[0]->full_name)->subject('Reset Password');
            });

            return Redirect::back()->with('success', 'Check mail successfully.');
        }
        else{
            return Redirect::back()->withErrors(["email"=>"We can't find a user with that e-mail address."])->withInput();
        }

        //dd($postData);
    }
    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);
        //dd($this->guard()->user()->role_type);

        if($this->guard()->user()->role_type == "Companies"){
            return redirect()->route('companies-dashboard');
        }
        else{
            return redirect()->route('admin-dashboard');
        }

        /*return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());*/
    }
}
