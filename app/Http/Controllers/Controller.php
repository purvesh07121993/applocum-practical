<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\ModulePages;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getArray($url,$role_id)
    {
    	$module_array = ModulePages::where('page_url',$url)->get();
        //dd($module_array);
    	$accessArray  = array();
        if(!empty($module_array)){

        $module_id    = $module_array[0]->fk_module_id;
    	DB::enableQueryLog();
    	$permission = ModulePages::where('role_permissions.fk_role_id',$role_id)
                    ->select(
                        'module_pages.page_url as page_url'
                    )
                    ->leftJoin('role_permissions', 'role_permissions.fk_page_id', '=', 'module_pages.page_id')
                    ->get();
        foreach ($permission->toarray() as $key => $value) {
                    array_push($accessArray, $value['page_url']);
        }
        }         
    	return $accessArray;
    }
}
