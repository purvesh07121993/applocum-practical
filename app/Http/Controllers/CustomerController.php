<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Routes;
use App\User;
use App\Roles;
use Input;
use Redirect;
use Auth;
use Validator;
use Mail;

class CustomerController extends Controller{
    //
    public function __construct(){
	    $this->middleware('admin');
	}

	public function index(){
        $accessData    = $this->getArray('customer-agent',Auth::user()->fk_role_id); 

		$userData = User::whereIn('role_type', ["Customer","Agent"])->where('status','1')->orderBy('user_id', 'DESC')->get();
		//dd($userData->toArray());
        return view('admin.customer_agent',compact('userData','accessData'));

       
	}


	public function customerAdd(){
        $accessData    = $this->getArray('customer-agent',Auth::user()->fk_role_id); 

		$roleData = Roles::whereIn('role_type', ["Customer","Agent"])->where('status','1')->get();
		//dd($userData->toArray());
        return view('admin.customer_agent_add',compact('roleData','accessData'));

	}


	public function saveCustomer(request $request){
        $accessData    = $this->getArray('customer-agent',Auth::user()->fk_role_id); 

		$loginUser       = Auth::user();
    	$this->validate($request, [
            'role' 		=> 'required|string',
	        'full_name' => 'required|string|max:255',
            'email' 	=> 'required|email|unique:users'
        ]);
        //dd(Input::get());

        $roleTypeData = Roles::where('role_id',Input::get('role'))->get();
        //dd($roleTypeData->toArray());
        //dd($roleTypeData[0]['role_type']);

        $password = mt_rand(1000000, 9999999);
        $data = User::create([
            'user_number' 	=> strtotime(date("Y-m-d")),
            'fk_role_id' 	=> Input::get('role'),
            'full_name' 	=> Input::get('full_name'),
            'email' 		=> Input::get('email'),
            'password' 		=> bcrypt($password),
            'role_type' 	=> $roleTypeData[0]['role_type'],
            'mobile' 		=> Input::get('mobile'),
            'company_name' 	=> Input::get('company_name'),
            'status' 			=> 1,
            'is_new_user' 		=> 1,
            'created_by' 		=> $loginUser['user_id']
        ]);
        //dd($data->toArray());

        $insertId = $data['user_id'];
        $User              = User::find($insertId);
        $User->user_number = "100".$insertId;
        $User->save();

        $user = User::findOrFail($insertId);

        Mail::send('emails.welcome', ['user' => $user,'password' => $password], function ($m) use ($user) {
           //$m->from('developerhemshub@gmail.com', 'PHP');
            $m->to($user->email, $user->full_name)->subject('Welcome to Digital Book Printing');
        });

	    return redirect()->route('customer-agent')->with('success', 'Customer has been added successfully.','accessData',$accessData);
	}


	public function customerUpdate($id){
        $accessData    = $this->getArray('customer-agent',Auth::user()->fk_role_id); 

		$userData = User::findOrFail($id);
		$roleData = Roles::whereIn('role_type', ["Customer","Agent"])->where('status','1')->get();
    	//dd($userData->toArray());
    	return view('admin.customer_agent_update')->with(['userData'=> $userData,'roleData'=>$roleData,'accessData'=>$accessData]);
	}


	public function saveUpdateCustomer(request $request, $id){
		$loginUser       = Auth::user();
    	$this->validate($request, [
            'role' 		=> 'required|string',
	        'full_name' => 'required|string|max:255',
            'email' 	=> 'required|email|unique:users,email,'.$id.',user_id'
        ]);
        //dd(Input::get());

        $roleTypeData = Roles::where('role_id',Input::get('role'))->get();

        User::where('user_id', $id)->update(array(
            'fk_role_id' 	=> Input::get('role'),
            'full_name' 	=> Input::get('full_name'),
            'email' 		=> Input::get('email'),
            'role_type' 	=> $roleTypeData[0]['role_type'],
            'mobile' 		=> Input::get('mobile'),
            'company_name' 	=> Input::get('company_name'),
            'updated_by' 	=> $loginUser['user_id']
        ));

        return redirect()->route('customer-agent')->with('success', 'Customer has been updated successfully.');
	}


	public function deleteCustomer($id){
       
		
        $user = User::find($id);
        $user->delete();
        return redirect()->route('customer-agent')->with('success', 'Customer has been deleted successfully.');
	}


    public function customerDetailsView($id){
        $accessData    = $this->getArray('customer-agent',Auth::user()->fk_role_id); 

        $userData = User::whereIn('users.role_type', ["Customer","Agent"])->where(['users.status'=>'1','users.user_id'=>$id])
                    ->select(
                        'users.*',
                        'roles.name as role_name'
                    )
                    ->leftJoin('roles', 'roles.role_id', '=', 'users.fk_role_id')
                    ->get();
        //dd($userData->toArray());
        return view('admin.customer_agent_details',compact('userData','accessData'));

      //  return view('admin.customer_agent_details')->with('userData', $userData);
    }
}
