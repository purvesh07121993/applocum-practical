<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Input;
use Validator;
use Redirect;
use Mail;
use Hash;
use App\User;
use App\Roles;
use Session;
class HomeController extends Controller{

	/**
	 * @author Purvesh Patel
	 * Created date: 
	 */
	public function __construct(){
		$this->middleware('checkLogin');
	}

	/**
	 * @author Purvesh Patel
	 * Created date: 
	 */
	public function index(){
		if(isset(Auth::user()->user_id) && Auth::user()->user_id != ""){
			return redirect()->route('dashboard');
		}
		else{
			return redirect()->route('sign-in');
		}
	}

	/**
	 * @author Purvesh Patel
	 * Created date: 
	 */
	public function signIn(){
		if(isset(Auth::user()->user_id) && Auth::user()->user_id != ""){
			return redirect()->route('quick-quote');
		}
		else{
			return view('signin');
		}
	}


	/**
	 * Get the guard to be used during registration.
	 *
	 * @return \Illuminate\Contracts\Auth\StatefulGuard
	 */
	protected function guard(){
		return Auth::guard();
	}
}