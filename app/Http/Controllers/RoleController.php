<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Routes;
use App\Roles;
use Input;
use Redirect;
use Auth;

class RoleController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth');

	}

    /**
	 * Descripation: use to admin
	 * @author Purvesh Patel
	 * Created date: 19 Desc 2018 10:30 AM
	 */
    public function index(){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 
      
        $roleData = Roles::orderBy('role_id', 'DESC')->get();
        return view('admin.role',compact('roleData','accessData'));

      //  return view('admin.role')->with('roleData', $roleData);
    	//return view('admin.role',compact('roleData','var2'));//->with('roleData', $roleData);
    	//return view('admin.role');
    }

    public function roleAdd(){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 
        return view('admin.role_add',compact('accessData'));

    	//return view('admin.role_add');
    }

    public function saveRole(Request $request){

        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        $user       = Auth::user();
    	$this->validate($request, [
	        'name' => 'required|string|max:255',
            'role_type' => 'required|in:Admin,Agent,Customer', //validate role input
        ]);

        $roleSlug = strtolower(str_replace(' ', '_', Input::get('name')));
        //dd($roleSlug);
        $role = new Roles;

	    $role->name        = Input::get('name');
	    $role->role_type   = Input::get('role_type');
        $role->status      = 1;
	    $role->role_slug   = $roleSlug;
	    $role->created_by  = $user['user_id'];
	    $role->save();

	    return redirect()->route('general-settings-role')->with('success', 'Role has been added successfully.','accessData',$accessData);
    }


    public function roleUpdate($id){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

    	$roleData = Roles::findOrFail($id);
    	//dd($roleData);
        return view('admin.role_update',compact('roleData','accessData'));

    //	return view('admin.role_update')->with('roleData', $roleData);
    	//return view('admin.role');
    }

    public function saveUpdateRole(Request $request, $id){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        $user       = Auth::user();
    	$this->validate($request, [
	        'name' => 'required|string|max:255',
            'role_type' => 'required|in:Admin,Agent,Customer', //validate role input
        ]);
        //dd($request);

        $roleSlug = strtolower(str_replace(' ', '_', Input::get('name')));

        $role              = Roles::find($id);
	    $role->name        = Input::get('name');
	    $role->role_type   = Input::get('role_type');
	    $role->status      = 1;
        $role->role_slug   = $roleSlug;
	    $role->updated_by  = $user['user_id'];
	    $role->save();

        return redirect()->route('general-settings-role')->with('success', 'Role has been updated successfully.','accessData',$accessData);
	   
    }


    /* Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteRole($id)
    {
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 
        
        $role = Roles::find($id);
        $role->delete();
        return redirect()->route('general-settings-role')->with('success', 'Role has been deleted successfully.','accessData',$accessData);
       
    }
    
}
