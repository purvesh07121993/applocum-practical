<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Routes;
use App\Roles;

use App\Modules;
use App\ModulePages;
use App\RolePermissions;
use Input;
use Redirect;
use DB;
use Auth;

class RolePermissionController extends Controller{
    
    public function __construct(){
	    $this->middleware('auth');
	}

    public function index(){
        $accessData    = $this->getArray('general-settings-role-permission',Auth::user()->fk_role_id); 

    	$rolePermissionData =  DB::table('role_permissions')
				->select('role_permissions.role_permissions_id','role_permissions.fk_role_id','role_permissions.fk_page_id','roles.role_id','roles.name')
				->join('roles','roles.role_id','=','role_permissions.fk_role_id')
				->groupBy('role_permissions.fk_role_id')
                ->orderBy('role_permissions_id', 'DESC')
				->get();
		//dd($rolePermissionData->toArray());
        return view('admin.role_permissions',compact('rolePermissionData','accessData'));

    	
    }

    public function rolePermissionAdd(){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        //$user = Auth::user();dd($user);dd($user['user_number']);
        $rolePermissionData =  DB::table('role_permissions')
                ->select('roles.role_id')
                ->join('roles','roles.role_id','=','role_permissions.fk_role_id')
                ->groupBy('role_permissions.fk_role_id')
                ->get();
        //dd($rolePermissionData->toArray('role_id'));
        $roleIddata = array();
        foreach($rolePermissionData as $row){
            $roleIddata[] = $row->role_id;
        }
        //dd($roleIddata);
        $roleData = DB::table('roles')
                    ->where('role_type','Admin')
                    ->whereNotIn('role_id', $roleIddata)
                    ->get();
        //dd($roleData->toArray());

        $modulesData = DB::table('modules')
                    ->where('status','1')
                    ->get();
        //dd($modulesData->toArray());

        $modulesPageData = array();
        foreach($modulesData as $valueModulesData){
            $modulesPagesData = DB::table('module_pages')
                    ->where('status','1')
                    ->where('fk_module_id',$valueModulesData->module_id)
                    ->get();
            //dd($modulesPages);
            foreach($modulesPagesData as $valueModulesPagesData){
                $modulesPageData[$valueModulesData->name][] = array(
                    "page_id" => $valueModulesPagesData->page_id,
                    "page_slug" => $valueModulesPagesData->page_slug,
                    "parent_page_id" => $valueModulesPagesData->parent_page_id,
                );
            }
        }

        //dd($modulesPageData);
        return view('admin.role_permissions_add',compact('roleData','accessData','modulesPageData'));

        // return view('admin.role_permissions_add')->with(['roleData'=> $rolesData,'modulesPageData'=>$modulesPageData]);
    }

    public function saveRolePermission(Request $request){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        $user       = Auth::user();
        $postData   = Input::all();
        $this->validate($request, [
            'role_type'     => 'required', //validate role input
            'page_ids'      => 'required', //validate role input
        ]);
        //dd($postData['page_ids']);
        $newArray = array();
        foreach ($postData['page_ids'] as $key => $value) {
            $newArray[] = array(
                "fk_role_id" => $postData['role_type'],
                "fk_page_id" => $value,
                "created_by" => $user['user_id']
            );
        }
        //dd($newArray);
        RolePermissions::insert($newArray);
        return redirect()->route('general-settings-role-permission')->with('success', 'Role Permissions has been added successfully.','accessData',$accessData);
    }

    public function rolePermissionUpdate($id){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        $rolePermissionUpdateData =  DB::table('role_permissions')
                ->where('fk_role_id',$id)
                ->get();
        $fk_page_ids = array();
        foreach($rolePermissionUpdateData as $rowPermissionUpdateData){
            $fk_page_ids[] = $rowPermissionUpdateData->fk_page_id;
        }
        //dd($fk_page_ids);

        $rolePermissionData =  DB::table('role_permissions')
                ->select('roles.role_id')
                ->join('roles','roles.role_id','=','role_permissions.fk_role_id')
                ->groupBy('role_permissions.fk_role_id')
                ->get();
        //dd($rolePermissionData->toArray('role_id'));
        $roleIddata = array();
        foreach($rolePermissionData as $row){
            $roleIddata[] = $row->role_id;
        }
        //dd($roleIddata);
        $rolesData = DB::table('roles')
                    ->where('role_type','Admin')
                    ->where('role_id', $id)
                    ->get();
        //dd($rolesData->toArray());

        $modulesData = DB::table('modules')
                    ->where('status','1')
                    ->get();
        //dd($modulesData->toArray());

        $modulesPageData = array();
        foreach($modulesData as $valueModulesData){
            $modulesPagesData = DB::table('module_pages')
                    ->where('status','1')
                    ->where('fk_module_id',$valueModulesData->module_id)
                    ->get();
            //dd($modulesPages);
            foreach($modulesPagesData as $valueModulesPagesData){
                $modulesPageData[$valueModulesData->name][] = array(
                    "page_id" => $valueModulesPagesData->page_id,
                    "page_slug" => $valueModulesPagesData->page_slug,
                    "parent_page_id" => $valueModulesPagesData->parent_page_id,
                );
            }
        }

        //dd($modulesPageData);
        return view('admin.role_permissions_update')->with(["currentData"=>$fk_page_ids,"role_id"=>$id,'roleData'=> $rolesData,'modulesPageData'=>$modulesPageData,'accessData'=>$accessData]);
    }


    public function saveUpdateRolePermission(Request $request, $id){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        $user       = Auth::user();
        $postData   = Input::all();
        $this->validate($request, [
            'role_type'     => 'required', //validate role input
            'page_ids'      => 'required', //validate role input
        ]);
        //dd($postData['page_ids']);
        RolePermissions::where('fk_role_id',$id)->delete();
        $newArray = array();
        foreach ($postData['page_ids'] as $key => $value) {
            $newArray[] = array(
                "fk_role_id" => $postData['role_type'],
                "fk_page_id" => $value,
                "updated_by" => $user['user_id']
            );
        }
        //dd($newArray);
        RolePermissions::insert($newArray);

        return redirect()->route('general-settings-role-permission')->with(['success', 'Role Permissions has been updated successfully.','accessData',$accessData]);
    }

    
    public function deletePermissionRole($id){
        $accessData    = $this->getArray('general-settings-role',Auth::user()->fk_role_id); 

        RolePermissions::where('fk_role_id',$id)->delete();
        return redirect()->route('general-settings-role-permission')->with(['success', 'Role Permissions has been deleted successfully.','accessData',$accessData]);
    }









    /*public function roleUpdate($id){
    	$roleData = Role::findOrFail($id);
    	//dd($roleData);
    	return view('admin.role_update')->with('roleData', $roleData);
    	//return view('admin.role');
    }

    public function saveUpdateRole(Request $request, $id){

    	$this->validate($request, [
	        'name' => 'required|string|max:255',
            'role_type' => 'required|in:Admin,Agent,Customer', //validate role input
        ]);
        //dd($request);

        $role = Role::find($id);


	    $role->name = Input::get('name');
	    $role->role_type = Input::get('role_type');
	    $role->status = 1;
	    $role->created_by = 1;
	    $role->updated_by = 1;
	    $role->save();

	    return redirect()->route('general-settings-role')->with('success', 'Role has been updated successfully.');
    }*/


    /* Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function deleteRole($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('general-settings-role')->with('success', 'Role has been deleted successfully.');
    }*/
}
