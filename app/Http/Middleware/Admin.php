<?php

namespace App\Http\Middleware;

use Closure;
use Auth; //at the top
use Route;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role_type == 'Admin') {
           return $next($request);
           // return redirect()->route('admin-dashboard');
        }
        else {
            return redirect()->route('login-design');
        }
    }
}
