<?php

namespace App\Http\Middleware;

use Closure;
use Auth; //at the top
use Route;

class Agent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role_type == 'Agent') {
            return $next($request);
        }
        elseif (Auth::check() && Auth::user()->role_type == 'Customer') {
            return redirect('/customer');
        }
        elseif (Auth::check() && Auth::user()->role_type == 'Admin') {
            return redirect()->route('admin-dashboard');
        }
        else {
            return redirect()->route('login-design');
        }
    }
}
