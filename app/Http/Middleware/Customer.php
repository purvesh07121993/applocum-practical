<?php

namespace App\Http\Middleware;

use Closure;
use Auth; //at the top
use Route;

class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role_type == 'Customer') {
            return $next($request);
        }
        elseif (Auth::check() && Auth::user()->role_type == 'Agent') {
            return redirect('/agent');
        }
        elseif (Auth::check() && Auth::user()->role_type == 'Admin') {
            return redirect()->route('admin-dashboard');
        }
        else {
            return redirect()->route('login-design');
        }
    }
}
