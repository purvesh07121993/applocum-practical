<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /*if (Auth::guard($guard)->check()) {
            return redirect('/dashboard');
        }*/
        //dd(Auth::guard()->user()->role_type);
        if (Auth::guard($guard)->check()) {

            if(Auth::guard()->user()->role_type == "Customer"){
                return redirect('/customer');
            }
            elseif (Auth::guard()->user()->role_type == "Agent"){
                return redirect('/agent');
            }
            else{
                return redirect()->route('admin-dashboard');
            }
        }

        return $next($request);
    }
}
