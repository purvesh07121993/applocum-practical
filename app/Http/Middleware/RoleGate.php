<?php

namespace App\Http\Middleware;

use Closure;

use App\Roles;
use Auth;
use DB;
use App\User;
use App\Modules;
use App\ModulePages;
use App\RolePermissions;

class RoleGate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        $user_role_id  = Auth::user()->fk_role_id;
        
            $permission    = DB::table('roles')
                ->join('role_permissions', 'roles.role_id', '=', 'role_permissions.fk_role_id')
                ->join('module_pages', 'module_pages.page_id', '=', 'role_permissions.fk_page_id')
                ->select('*')
                ->where('roles.role_id',$user_role_id)
                ->where('module_pages.page_url', 'LIKE', $role)
                ->get();
        //  dd($permission);
        if(!empty($permission->toArray())){
         //print_r($request);exit();
                return $next($request,$permission);
            
        }else{
                return redirect('/dashboard');

        }
        
    }
    public function getModuleList($module = '')
    {
        return 0;exit();
          

      //  echo $module;exit();
    }
}
