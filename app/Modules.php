<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $primaryKey = 'module_id';

    protected $fillable = ['name', 'status'];
}
