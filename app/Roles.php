<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //
    protected $primaryKey = 'role_id';

    protected $fillable = ['name', 'role_type', 'status', 'created_by', 'updated_by'];
}
