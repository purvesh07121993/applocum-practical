<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_number','role_type','full_name', 'email', 'password','fk_role_id','mobile', 'photo', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at', 'remember_token'
    ];

   /* protected $guarded = ['mobile', 'photo', 'gender', 'company_name', 'street_address', 'city', 'state', 'country', 'zipcode', 'latitude', 'longitude', 'status', 'security_token', 'token_expiry_date', 'is_new_user', 'created_by', 'updated_by', 'created_at', 'updated_at', 'remember_token'];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
