
$(document).ready(function() {
 $(".navigation").navigation({
  responsive:true,
  mobileBreakpoint:768,
  showDuration:300,
  hideDuration:300,
  showDelayDuration:100,
  hideDelayDuration:0,
  submenuTrigger:"hover",
  effect:"fade",
  submenuIndicator:true,
  hideSubWhenGoOut:true,
  visibleSubmenusOnMobile:false,
  fixed:false,
  overlay:true,
  overlayColor:"rgba(0, 0, 0, 0.5)",
  hidden:false,
  offCanvasSide:"left",
}); 
});


$(".service-block .img").click(function(event) {
   $(this).parents('.design-items ul li').addClass('selected');
   $(this).parents('.design-items ul li').siblings('li').removeClass('selected');
});

$(function() {
  //----- OPEN
  $('[data-popup-open]').on('click', function(e)  {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
    e.preventDefault();
  });

  //----- CLOSE
  $('[data-popup-close]').on('click', function(e)  {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
    e.preventDefault();
  });

   });

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});


$(document).ready(function () {
    var navListItems = $('div.setup-panel a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allBackBtn = $('.backBtn');
        allWells.hide();
      navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('checked').addClass('checked');
            $item.addClass('checked');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            radiocheck = curStep.find("input[type='radio']"),
            isValid = true;
        if ($(radiocheck).is(":checked")) {
          isValid = true; 
        }
        else {
           isValid = false;
           //e.stopPropagation();
           //alert("Please Select Service");
           iziToast.error({
             title: 'Error',
             message: 'Please Select Service',
             timeout: 2000,
             position: 'topRight',
         });
        }
        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
      });
        allBackBtn.click(function () {
          $(this).parent().hide();
          $(this).parent().prev().show();
        });
    $('div.setup-panel a.checked').trigger('click');
});


$(document).ready(function() {
  var readURL = function(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('.profile-pic').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $(".file-upload").on('change', function(){
    readURL(this);
  });
  $(".upload-button").on('click', function() {
    $(".file-upload").click();
  });
});