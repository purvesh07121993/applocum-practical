var DropzoneDemo = {
	init: function () {
		Dropzone.options.mDropzoneOne = {
			paramName: "file",
			maxFiles: 1,
			maxFilesize: 5,
			addRemoveLinks: !0,
			acceptedFiles: "image/*",
		}, Dropzone.options.mDropzoneTwo = {
			paramName: "file",
			maxFiles: 10,
			maxFilesize: 150,
			addRemoveLinks: !0,
			//acceptedFiles: "/*",
		}, Dropzone.options.mDropzoneThree = {
			paramName: "file",
			maxFiles: 10,
			maxFilesize: 5,
			addRemoveLinks: !0,
			acceptedFiles: "image/*",
			
		}
	}
};
DropzoneDemo.init();