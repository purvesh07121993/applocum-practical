<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->bigInteger('user_number')->unique();
            $table->integer('fk_role_id');
            $table->enum('role_type', ['Admin', 'Companies']);
            $table->string('full_name',100);
            $table->string('email');
            $table->string('password');
            $table->string('mobile',20);
            $table->string('photo');
            $table->tinyInteger('status')->comment('0 = InActive , 1 = Active')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
