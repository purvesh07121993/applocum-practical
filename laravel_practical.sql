-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2019 at 05:15 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_practical`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = InActive , 1 = Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `name`, `status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Manage Companies', 1, '2018-12-21 02:00:00', '2018-12-21 02:00:00', NULL),
(2, 'Manage Employees', 1, '2018-12-21 02:00:00', '2018-12-21 02:00:00', NULL),
(3, 'Manage Role', 1, '2018-12-21 02:00:00', '2018-12-21 02:00:00', NULL),
(4, 'Manage Role Permission', 1, '2018-12-21 02:00:00', '2018-12-21 02:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_pages`
--

CREATE TABLE `module_pages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `fk_module_id` int(11) NOT NULL,
  `parent_page_id` int(11) NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_menu` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = No , 1 = Yes',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = InActive , 1 = Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_pages`
--

INSERT INTO `module_pages` (`page_id`, `fk_module_id`, `parent_page_id`, `page_title`, `page_description`, `page_slug`, `page_icon`, `page_url`, `is_menu`, `status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 1, 0, 'Manage Customer', 'All permissions Manage Customer', 'All', '', 'customer-agent', 1, 1, NULL, NULL, NULL),
(2, 1, 1, 'List Customer', 'List of Customer view only', 'View', '', 'customer-agent', 0, 1, NULL, NULL, NULL),
(3, 1, 1, 'Add Customer', 'Add Customer', 'Add', '', 'customer-add', 0, 1, NULL, NULL, NULL),
(4, 1, 1, 'Update Customer', 'Update Customer', 'Update', '', 'customer-update', 0, 1, NULL, NULL, NULL),
(5, 1, 1, 'Delete Customer', 'Delete Customer', 'Delete', '', 'customer-delete', 0, 1, NULL, NULL, NULL),
(6, 2, 0, 'Manage Users', 'All permissions Manage Users', 'All', '', 'admin-user', 1, 1, NULL, NULL, NULL),
(7, 2, 6, 'List Users', 'List of Users view only', 'View', '', 'admin-user', 0, 1, NULL, NULL, NULL),
(8, 2, 6, 'Add Users', 'Add Users', 'Add', '', 'admin-add', 0, 1, NULL, NULL, NULL),
(9, 2, 6, 'Update Users', 'Update Users', 'Update', '', 'admin-update', 0, 1, NULL, NULL, NULL),
(10, 2, 6, 'Delete Users', 'Delete Users', 'Delete', '', 'admin-delete', 0, 1, NULL, NULL, NULL),
(11, 3, 0, 'Manage Role', 'All permissions Manage Role', 'All', '', 'general-settings-role', 1, 1, NULL, NULL, NULL),
(12, 3, 11, 'List Role', 'List of Role view only', 'View', '', 'general-settings-role', 0, 1, NULL, NULL, NULL),
(13, 3, 11, 'Add Role', 'Add Role', 'Add', '', 'general-settings-role-add', 0, 1, NULL, NULL, NULL),
(14, 3, 11, 'Update Role', 'Update Role', 'Update', '', 'general-settings-role-update', 0, 1, NULL, NULL, NULL),
(15, 3, 11, 'Delete Role', 'Delete Role', 'Delete', '', 'general-settings-role-delete', 0, 1, NULL, NULL, NULL),
(16, 4, 0, 'Manage Role Permission', 'All permissions Manage Role', 'All', '', 'general-settings-role-permission', 1, 1, NULL, NULL, NULL),
(17, 4, 16, 'List Role Permission', 'List of Role Permission view only', 'View', '', 'general-settings-role-permission', 0, 1, NULL, NULL, NULL),
(18, 4, 16, 'Add Role Permission', 'Add Role Permission', 'Add', '', 'general-settings-role-permission-add', 0, 1, NULL, NULL, NULL),
(19, 4, 16, 'Update Role Permission', 'Update Role Permission', 'Update', '', 'general-settings-role-Permission-update', 0, 1, NULL, NULL, NULL),
(20, 4, 16, 'Delete Role Permission', 'Delete Role Permission', 'Delete', '', 'general-settings-role-permission-delete', 0, 1, NULL, NULL, NULL),
(21, 2, 6, 'Users details', 'Users details', 'Details', '', 'admin-details', 0, 1, NULL, NULL, NULL),
(22, 1, 1, 'Details Customer', 'Details Customer', 'Details', '', 'customer-details', 0, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_type` enum('Admin','Companies') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = InActive , 1 = Active',
  `role_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `role_type`, `status`, `role_slug`, `created_by`, `updated_by`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Admin', 'Admin', 1, 'admin', 1, 0, '2019-01-06 19:35:47', '2019-01-06 19:35:47', NULL),
(2, 'Companies', 'Companies', 1, 'companies', 1, 0, '2019-01-06 19:36:09', '2019-01-06 19:36:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `role_permissions_id` int(10) UNSIGNED NOT NULL,
  `fk_role_id` int(11) NOT NULL,
  `fk_page_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`role_permissions_id`, `fk_role_id`, `fk_page_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `remember_token`) VALUES
(10, 1, 1, 0, 1, NULL, NULL, NULL),
(11, 1, 2, 0, 1, NULL, NULL, NULL),
(12, 1, 3, 0, 1, NULL, NULL, NULL),
(13, 1, 4, 0, 1, NULL, NULL, NULL),
(14, 1, 5, 0, 1, NULL, NULL, NULL),
(15, 1, 22, 0, 1, NULL, NULL, NULL),
(16, 1, 6, 0, 1, NULL, NULL, NULL),
(17, 1, 7, 0, 1, NULL, NULL, NULL),
(18, 1, 8, 0, 1, NULL, NULL, NULL),
(19, 1, 9, 0, 1, NULL, NULL, NULL),
(20, 1, 10, 0, 1, NULL, NULL, NULL),
(21, 1, 21, 0, 1, NULL, NULL, NULL),
(22, 1, 11, 0, 1, NULL, NULL, NULL),
(23, 1, 12, 0, 1, NULL, NULL, NULL),
(24, 1, 13, 0, 1, NULL, NULL, NULL),
(25, 1, 14, 0, 1, NULL, NULL, NULL),
(26, 1, 15, 0, 1, NULL, NULL, NULL),
(27, 1, 16, 0, 1, NULL, NULL, NULL),
(28, 1, 17, 0, 1, NULL, NULL, NULL),
(29, 1, 18, 0, 1, NULL, NULL, NULL),
(30, 1, 19, 0, 1, NULL, NULL, NULL),
(31, 1, 20, 0, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_number` bigint(20) NOT NULL,
  `fk_role_id` int(11) NOT NULL,
  `role_type` enum('Admin','Companies') COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = InActive , 1 = Active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_number`, `fk_role_id`, `role_type`, `full_name`, `email`, `password`, `mobile`, `photo`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 1001, 1, 'Admin', 'Applocum Admin', 'applocumadmin@yopmail.com', '$2y$10$Vy7SQQy5pqi3MD4mij5SE.wZ7TmMIoPVXqlkPQNE.NB7xzcUYQfKa', '9876543210', '1569941487wwi-fav.png', 1, 1, 1, '2018-12-11 13:00:00', '2019-10-01 09:22:41', 'ZXvwK1zyTdvc3Yt6aOHNtbWBkMz2U1rox9b5B1DUdrfE9mtvdXbaSZbKniA3'),
(17, 10017, 2, 'Companies', 'Purvesh Patel', 'kipurvesh.php@gmail.com', '$2y$10$kDKDhM.yr0trIKuwIPhi2.aJ1GjGVlRXTZKU3Gh6F.xJkXwPr7LAu', '09722552298', '1569941529wwi-fav.png', 1, 1, 0, '2019-10-01 09:22:09', '2019-10-01 09:22:09', NULL),
(18, 10018, 2, 'Companies', 'Applocum Company', 'applocumcompany@yopmail.com', '$2y$10$U4b0ulmRnw83g6.Kb7FPc.TAYetGoxsimzWrduwKrPKQi3KysQmaW', '09722552298', '1569942146wwi-fav.png', 1, 1, 1, '2019-10-01 09:27:23', '2019-10-01 09:32:26', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_pages`
--
ALTER TABLE `module_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`role_permissions_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_user_number_unique` (`user_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `module_pages`
--
ALTER TABLE `module_pages`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `role_permissions_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
