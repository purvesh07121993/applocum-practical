@section('page-title', 'Companies Details')
@extends('elements.admin_master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title ">Companies</h3>			
			</div>
			<div class="ml-3">
				<a href="{{ route('admin-user') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-arrow-left"></i><span>Back</span></span></a>
			</div>
		</div>
	</div>
	<!-- END: Subheader -->		        
	<div class="m-content">
		<!--Begin::Section-->
		@if (\Session::get('success'))
		<div class="alert alert-success" role="alert" id="flash_message">
			{{ \Session::get('success') }}
		</div>
		@endif
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<div class="m-portlet">
					<div class="m-portlet__body">
						<div class="m-card-profile">
							<div class="m-card-profile__title m--hide">
								Profile
							</div>
							<div class="m-card-profile__pic">
								<div class="m-card-profile__pic-wrapper">
									
									@if($userData[0]['photo'] != "")
									<img src="{{ URL::asset('assets/uploads/user/').'/'.$userData[0]['photo'] }}" class="m--img-rounded m--marginless" alt=""/>
									
									@else
									<img src="{{ URL::asset('assets/images/users/user4.jpg') }}" class="m--img-rounded m--marginless" alt=""/>
									
									@endif
								</div>
							</div>
							<div class="m-card-profile__details">
								<span class="m-card-profile__name">{{ $userData[0]['full_name'] }}</span>
								<span class="m-card-profile__email m-link">{{ $userData[0]['role_name'] }}</span><br>
								<a href="" class="m-card-profile__email m-link">ID: {{ $userData[0]['user_number'] }}</a>
							</div>
						</div>	
						
					</div>			
				</div>	
			</div>
			<div class="col-xl-9 col-lg-8">
				<div class="m-portlet  m-portlet--bordered-semi m-portlet--space">
					<div class="m-portlet__body">
						<h5 class="m-portlet__head-text">Personal Info</h5>
						<!--begin::Widget 29-->
						<div class="m-widget29">       
							<div class="m-widget_content p-0">
								<div class="m-widget_content-items">
									<div class="m-widget_content-item pr-3">
										<span>Email Address</span>
										<span>{{ $userData[0]['email'] }}</span>
									</div>
									{{--  --}}
									
									<div class="m-widget_content-item">
										<span>Mobile Number</span>
										<span>{{ $userData[0]['mobile'] }}</span>
									</div>
								</div>
								<div class="m-separator m-separator--dashed"></div>
								<div class="m-widget_content-items">
									<div class="m-widget_content-item">
										<span>created date</span>
										<span style="color: #575962; font-size: 14px;">
											{{ $userData[0]['created_at'] }}
										</span>
									</div>  
								</div>
							</div>
						</div>
						<!--end::Widget 29--> 
					</div>
				</div>
			</div>
		</div>		        
	</div>
</div>
@endsection