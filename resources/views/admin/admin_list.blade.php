@section('page-title', 'Companies List')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title">Companies</h3>      
			</div>
			@if(in_array('admin-add',$accessData))
			<div class="ml-3">
				<a href="{{ route('admin-add') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-plus"></i><span>Add Companies</span></span></a>
			</div>
			@endif

		</div>
	</div>

	<div class="m-content">
		<!--Begin::Section-->
		@if (\Session::get('success'))
		<div class="alert alert-success" role="alert" id="flash_message">
			{{ \Session::get('success') }}
		</div>
		@endif  

		<!--begin::Portlet-->
		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">List</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->
			<div class="m-portlet__body">
				<div class="table-responsive">
					<table class="table table-striped- table-bordered table-hover table-checkable datatable">
						<thead>
							<tr>
								<th>Photo</th>
								<th>User Number</th>
								<th>Full Name</th>
								<th>Role Type</th>
								<th>Email</th>
								<th>Phone Number</th>
								<th width="15%">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($userData as $row)
							<tr>
								<td>
									@if($row->photo != "")
										<img src="{{ URL::asset('assets/uploads/user/').'/'.$row->photo }}" class="m--img-rounded m--marginless" alt="" style="width: 80px;" />
									@else
										<img src="{{ URL::asset('assets/images/users/user4.jpg') }}" class="m--img-rounded m--marginless" alt="" style="width: 80px;"/>
									@endif
								</td>
								<td>{{ $row->user_number }}</td>
								<td>{{ $row->full_name }}</td>
								<td>{{ $row->role_type }}</td>
								<td>{{ $row->email }}</td>
								<td>{{ $row->mobile }}</td>
								<td>
									@if(in_array('admin-update',$accessData))

									<a href="{{ URL::route('admin-update', $row->user_id) }}" class="btn btn-accent m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="Edit"><i class="la la-pencil"></i></a>
									@endif
									@if(in_array('admin-delete',$accessData))
									<a href="#" class="btn btn-danger m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="Delete" onclick="deleteRole('{{ $row->user_id }}');"><i class="la la-trash-o"></i></a>
									@endif
									@if(in_array('admin-details',$accessData))
									<a href="{{ URL::route('admin-details', $row->user_id) }}" class="btn btn-info m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="View"><i class="la la-eye"></i></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Body -->

@endsection

@section('content_js')
<script type="text/javascript">
	function deleteRole(id){
		swal({
			title: "Are you sure?",
			text: "You want to permanently delete this Companies.",
			type: "warning",
			showCancelButton: !0,
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel!",
			reverseButtons: 0
		}).then(function (e) {
			e.value ? window.location = "admin-user/"+id+"/delete" : "cancel" === e.dismiss && swal("Cancelled", "Companies user are safe.", "error")
		})
	};
</script>
@endsection