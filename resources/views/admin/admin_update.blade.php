@section('page-title', 'Companies Update')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title">Companies</h3>      
			</div>
			<div class="ml-3">
				<a href="{{ route('admin-user') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-arrow-left"></i><span>Back</span></span></a>
			</div>
		</div>
	</div>
	<div class="m-content">
		<!--Begin::Section-->
		<!--begin::Portlet-->
		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">Update</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->

			{!! Form::open(array('route' => ['admin-save-update', $userData->user_id],'method'=>'POST','files'=>'true','class'=>'m-login__form m-form')) !!}
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Role <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							
							<select class="form-control m-input" name="role">
								<option value="">Select Role</option>

								@foreach($roleData as $valueRoleData)

									<option value="{{ $valueRoleData->role_id }}" @if(old('role',$userData->fk_role_id) ==  $valueRoleData->role_id) selected @endif > {{ $valueRoleData->name }}
                                    </option>
								@endforeach
							</select>
							@if ($errors->has('role'))
							<div class="text-danger">
								{{ $errors->first('role') }}
							</div>
							@endif
						</div>
					</div>
				</div>

				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Full Name {{old('full_name')}} <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							

							{!! Form::text('full_name',old("full_name", $userData->full_name ?: ''),['id'=>'full_name','class'=>'form-control m-input','placeholder'=>'Full Name']) !!}
							
							@if ($errors->has('full_name'))
				                <div class="text-danger">
				                    {{ $errors->first('full_name') }}
				                </div>
				            @endif

						</div>
					</div>
				</div>


				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Email <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							{!! Form::email('email',old("email", $userData->email ?: ''),['id'=>'email','class'=>'form-control m-input','placeholder'=>'Email']) !!}

                            @if ($errors->has('email'))
				                <div class="text-danger">
				                    {{ $errors->first('email') }}
				                </div>
				            @endif
						</div>
					</div>
				</div>

				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Phone Number</label>
						<div class="col-lg-10">
							{!! Form::text('mobile',old("mobile", $userData->mobile ?: ''),['id'=>'mobile','class'=>'form-control m-input','placeholder'=>'Phone Number']) !!}
						</div>
					</div>
				</div>

				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Logo</label>
						<div class="col-lg-10">
							{!! Form::file('photo',['id'=>'photo','class'=>'form-control m-input']) !!}
							{!! Form::hidden('photo_hidden', $userData->photo , array('id' => 'photo_hidden')) !!}
							@if ($errors->has('photo'))
				                <div class="text-danger">
				                    {{ $errors->first('photo') }}
				                </div>
				            @endif
						</div>
					</div>
				</div>

				
				<div class="m-form__seperator m-form__seperator--dashed"></div>

			
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-6">
								{!! Form::button('Update Companies',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-primary m-btn--air','value'=>'Update Admin']) !!}

								<a href="{{ route('admin-user') }}" class="btn btn-secondary m-btn--air">Cancel</a>
							</div>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>  
		<!--end::Portlet-->
		<!--End::Section--> 
	</div>
</div>
<!-- end:: Body -->
@endsection