@section('page-title', 'Change Password')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<!-- <h3 class="m-subheader__title">Change Password</h3> -->
			</div>
			<div class="ml-3">
				<a href="{{ route('admin-dashboard') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-arrow-left"></i><span>Back</span></span></a>
			</div>
		</div>
	</div>
	<div class="m-content">
		@if (\Session::get('error'))
		<div class="alert alert-danger" role="alert" id="flash_message">
			{{ \Session::get('error') }}
		</div>
		@endif

		@if (\Session::get('success'))
		<div class="alert alert-success" role="alert" id="flash_message">
			{{ \Session::get('success') }}
		</div>
		@endif

		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">Change Password</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->

			{!! Form::open(array('route' => 'change-password-save','method'=>'POST','files'=>'true','class'=>'m-login__form m-form')) !!}
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Current Password <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							{!! Form::password('current_password',['id'=>'current_password','class'=>'form-control m-input','placeholder'=>'Current Password']) !!}

							@if ($errors->has('current_password'))
							<div id="name_error" class="text-danger">
								{{ $errors->first('current_password') }}
							</div>
							@endif
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">New Password <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							{!! Form::password('new_password',['id'=>'new_password','class'=>'form-control m-input','placeholder'=>'New Password']) !!}
							@if ($errors->has('new_password'))
							<div id="name_error" class="text-danger">
								{{ $errors->first('new_password') }}
							</div>
							@endif
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Confirm Password <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							{!! Form::password('confirm_password',['id'=>'confirm_password','class'=>'form-control m-input','placeholder'=>'Confirm Password']) !!}
							@if ($errors->has('confirm_password'))
							<div id="name_error" class="text-danger">
								{{ $errors->first('confirm_password') }}
							</div>
							@endif
						</div>
					</div>
				</div>
				<div class="m-form__seperator m-form__seperator--dashed"></div>
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-6">
								{!! Form::button('Change Password',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-primary m-btn--air','value'=>'Change Password']) !!}

								<a href="{{ route('admin-dashboard') }}" class="btn btn-secondary m-btn--air">Cancel</a>
							</div>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>  
		<!--end::Portlet-->
		<!--End::Section--> 
	</div>
</div>
<!-- end:: Body -->
@endsection