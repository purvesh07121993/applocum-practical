@section('page-title', 'My Profile')
@extends('elements.admin_master')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title ">My Profile</h3>			
			</div>
		</div>
	</div>
	<!-- END: Subheader -->		        
	<div class="m-content">
		<!--Begin::Section-->
		@if (\Session::get('success'))
		<div class="alert alert-success" role="alert" id="flash_message">
			{{ \Session::get('success') }}
		</div>
		@endif
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<div class="m-portlet">
					<div class="m-portlet__body">
						<div class="m-card-profile">
							<div class="m-card-profile__title m--hide">
								Your Profile
							</div>
							<div class="m-card-profile__pic">
								<div class="m-card-profile__pic-wrapper">
									
									@if(Auth::user()->photo != "")
										<img src="{{ URL::asset('assets/uploads/user').'/'.Auth::user()->photo }}" class="m--img-rounded m--marginless" id="profile-img-tag" alt=""/>
									
									@else
										<img src="{{ URL::asset('assets/images/users/user4.jpg') }}" class="m--img-rounded m--marginless" id="profile-img-tag" alt=""/>
									
									@endif
								</div>
							</div>
							<div class="m-card-profile__details">
								<span class="m-card-profile__name">{{ Auth::user()->full_name }}</span>
								<a href="" class="m-card-profile__email m-link">{{ Auth::user()->email }}</a>
							</div>
						</div>	
						
					</div>			
				</div>	
			</div>
			<div class="col-xl-9 col-lg-8">
				<div class="m-portlet m-portlet--tabs">
					<div class="m-portlet__head">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
										<i class="flaticon-share m--hide"></i>
										Update Profile
									</a>
								</li>
								<!-- <li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
										Change Password
									</a>
								</li> -->
							</ul>
						</div>
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="m_user_profile_tab_1">
							{!! Form::open(array('route' => ['admin-profile-update', Auth::user()->user_id],'method'=>'POST','files'=>'true','class'=>'m-form m-form--fit m-form--label-align-left')) !!}
								<div class="m-portlet__body">
									
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">1. Personal Details</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">Full Name <storage class="text-danger">*</storage></label>
										<div class="col-7">
											{!! Form::text('full_name',old("full_name", Auth::user()->full_name ?: ''),['id'=>'full_name','class'=>'form-control m-input','placeholder'=>'Full Name']) !!}
											@if ($errors->has('full_name'))
											<div id="name_error" class="text-danger">
												{{ $errors->first('full_name') }}
											</div>
											@endif
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">Email <storage class="text-danger">*</storage></label>
										<div class="col-7">
											{!! Form::email('email',old("email", Auth::user()->email ?: ''),['id'=>'email','class'=>'form-control m-input','placeholder'=>'Email']) !!}

			                                @if ($errors->has('email'))
											<div id="name_error" class="text-danger">
												{{ $errors->first('email') }}
											</div>
											@endif
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">Phone Number</label>
										<div class="col-7">
											{!! Form::text('mobile',old("mobile", Auth::user()->mobile ?: ''),['id'=>'mobile','class'=>'form-control m-input','placeholder'=>'Phone Number']) !!}
										</div>
									</div>

									

									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-3 col-form-label">Photo</label>
										<div class="col-7">
											{!! Form::file('photo',['id'=>'photo','class'=>'form-control m-input']) !!}

											{!! Form::hidden('photo_hidden', Auth::user()->photo, array('id' => 'photo_hidden')) !!}

											@if ($errors->has('photo'))
								                <div class="text-danger">
								                    {{ $errors->first('photo') }}
								                </div>
								            @endif
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-3">
											</div>
											<div class="col-7">
												{!! Form::button('Save changes',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-primary m-btn--air','value'=>'Save changes']) !!}

												<a href="{{ route('admin-dashboard') }}" class="btn btn-secondary m-btn--air">Cancel</a>
											</div>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
						
					</div>
				</div>
			</div>
		</div>		        
	</div>
</div>
@endsection

@section('content_js')
<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo").change(function(){
        readURL(this);
    });
</script> 
@endsection