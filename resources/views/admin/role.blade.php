@section('page-title', 'Role')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title">Role</h3>      
			</div>
			@if(in_array('general-settings-role-add',$accessData))
			<div class="ml-3">
				<a href="{{ route('general-settings-role-add') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-plus"></i><span>Add Role</span></span></a>
			</div>
			@endif
		</div>
	</div>

	<div class="m-content">
		<!--Begin::Section-->
		@if (\Session::get('success'))
		<div class="alert alert-success" role="alert" id="flash_message">
			{{ \Session::get('success') }}
		</div>
		@endif  

		<!--begin::Portlet-->
		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">List</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->
			<div class="m-portlet__body">
				<div class="table-responsive">
					<table class="table table-striped- table-bordered table-hover table-checkable datatable">
						<thead>
							<tr>
								<th>Role Name</th>
								<th>Role Type</th>
								<th width="20%">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($roleData as $row)
							<tr>
								<td>{{ $row->name }}</td>
								<td>{{ $row->role_type }}</td>
								<td>
									@if(in_array('general-settings-role-update',$accessData))
									<a href="{{ URL::route('general-settings-role-update', $row->role_id) }}" class="btn btn-accent m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="Edit"><i class="la la-pencil"></i></a>
									@endif
									@if(in_array('general-settings-role-delete',$accessData))
									<a href="#" class="btn btn-danger m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="Delete" onclick="deleteRole('{{ $row->role_id }}');"><i class="la la-trash-o"></i></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Body -->
@endsection


@section('content_js')
<script type="text/javascript">
	/*function deleteRole(id){
		swal({
			title: "Are you sure?",
			text: "You want to permanently delete this role.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plx!"
		}, function (isConfirm) {
			alert(1);
			if (isConfirm) {
                //window.location = "sub_category_delete?id="+id;
                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                alert(1);
            } else {
                //swal("Cancelled", "Role are safe.", "error");
                alert(2);
            }
        });
	}*/

	function deleteRole(id){
		swal({
			title: "Are you sure?",
			text: "You want to permanently delete this role.",
			type: "warning",
			showCancelButton: !0,
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel!",
			reverseButtons: 0
		}).then(function (e) {
			e.value ? window.location = "role/"+id+"/delete" : "cancel" === e.dismiss && swal("Cancelled", "Role are safe.", "error")
		})
	};

	/*
	function deleteRole(id){
		swal({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			type: "warning",
			showCancelButton: !0,
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel!",
			reverseButtons: !0
		}).then(function (e) {
			e.value ? swal("Deleted!", "Your file has been deleted.", "success") : "cancel" === e.dismiss && swal("Cancelled", "Your imaginary file is safe :)", "error")
		})
	};
	*/

</script>
@endsection