@section('page-title', 'Role Add')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title">Role</h3>      
			</div>
			<div class="ml-3">
				<a href="{{ route('general-settings-role') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-arrow-left"></i><span>Back</span></span></a>
			</div>
		</div>
	</div>
	<div class="m-content">
		<!--Begin::Section-->
		<!--begin::Portlet-->
		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">Add</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->

			{!! Form::open(array('route' => 'general-settings-role-save','method'=>'POST','files'=>'true','class'=>'m-login__form m-form')) !!}
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Role Name <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							{!! Form::text('name',null,['id'=>'name','class'=>'form-control m-input','placeholder'=>'Role Name']) !!}
							@if ($errors->has('name'))
							<div id="name_error" class="text-danger">
								{{ $errors->first('name') }}
							</div>
							@endif
						</div>
					</div>
				</div>

				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Role Type <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							{!! Form::select('role_type', array('Select Role Type' => 'Select Role Type', 'Admin' => 'Admin', 'Agent' => 'Agent', 'Customer' => 'Customer'), 'Select Role Type', array('class' => 'form-control m-input') ) !!}
							
							@if ($errors->has('role_type'))
							<div id="role_type_error" class="text-danger">
								{{ $errors->first('role_type') }}
							</div>
							@endif
						</div>
					</div>
				</div>
			<div class="m-form__seperator m-form__seperator--dashed"></div>

			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							{!! Form::button('Add Role',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-primary m-btn--air','value'=>'Add Role']) !!}

							<a href="{{ route('general-settings-role') }}" class="btn btn-secondary m-btn--air">Cancel</a>
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>  
		<!--end::Portlet-->
		<!--End::Section--> 
	</div>
</div>
<!-- end:: Body -->
@endsection