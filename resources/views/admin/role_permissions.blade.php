@section('page-title', 'Role Permission')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title">Role Permission</h3>      
			</div>
			@if(in_array('general-settings-role-permission-add',$accessData))
			<div class="ml-3">
				<a href="{{ route('general-settings-role-permission-add') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-plus"></i><span>Add Role Permission</span></span></a>
			</div>
			@endif
		</div>
	</div>

	<div class="m-content">
		<!--Begin::Section-->
		@if (\Session::get('success'))
		<div class="alert alert-success" role="alert" id="flash_message">
			{{ \Session::get('success') }}
		</div>
		@endif  

		<!--begin::Portlet-->
		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">List</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->
			<div class="m-portlet__body">
				<div class="table-responsive">
					<table class="table table-striped- table-bordered table-hover table-checkable datatable">
						<thead>
							<tr>
								<th>Role Name</th>
								<th width="20%">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($rolePermissionData as $row)
							<tr>
								<td>{{ $row->name }}</td>
								<td>
									@if(in_array('general-settings-role-Permission-update',$accessData))
									<a href="{{ URL::route('general-settings-role-Permission-update', $row->fk_role_id) }}" class="btn btn-accent m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="Edit"><i class="la la-pencil"></i></a>
									@endif
									@if(in_array('general-settings-role-permission-delete',$accessData))
									<a href="#" class="btn btn-danger m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill m-btn--air btn-sm" data-toggle="m-tooltip" title="" data-original-title="Delete" onclick="deleteRole('{{ $row->fk_role_id }}');"><i class="la la-trash-o"></i></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Body -->
@endsection

@section('content_js')
<script type="text/javascript">
	function deleteRole(id){
		swal({
			title: "Are you sure?",
			text: "You want to permanently delete this role Permission.",
			type: "warning",
			showCancelButton: !0,
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel!",
			reverseButtons: 0
		}).then(function (e) {
			e.value ? window.location = "role-permission/"+id+"/delete" : "cancel" === e.dismiss && swal("Cancelled", "Role Permission are safe.", "error")
		})
	};
</script>
@endsection