@section('page-title', 'Role Permission')
@extends('elements.admin_master')
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title">Role Permission</h3>      
			</div>
			<div class="ml-3">
				<a href="{{ route('general-settings-role-permission') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air btn-sm"><span><i class="la la-arrow-left"></i><span>Back</span></span></a>
			</div>
		</div>
	</div>
	<div class="m-content">
		<!--Begin::Section-->
		<!--begin::Portlet-->
		<div class="m-portlet" m-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">Update</h3>
					</div>      
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a> 
						</li>
						<li class="m-portlet__nav-item">
							<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>  
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->
			
			{!! Form::open(array('route' => ['general-settings-role-permission-save-update', $role_id],'method'=>'POST','files'=>'true','class'=>'m-login__form m-form')) !!}
			<div class="m-portlet__body">
				<div class="form-group m-form__group row">
					<label class="col-lg-2 col-form-label">Role  <storage class="text-danger">*</storage></label>
					<div class="col-lg-10">
						<select class="form-control m-input" name="role_type">
							<!-- <option value="" selected="selected">Select Role</option> -->
							@foreach($roleData as $valueRoleData)
							<option value="{{ $valueRoleData->role_id }}">{{ $valueRoleData->name }}</option>
							@endforeach
						</select>
						@if ($errors->has('role_type'))
						<div class="text-danger">
							{{ $errors->first('role_type') }}
						</div>
						@endif
					</div>
				</div>

				<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Assign Permission  <storage class="text-danger">*</storage></label>
						<div class="col-lg-10">
							@foreach($modulesPageData as $key => $valueModulesPageData)
							<div class="m-demo">
								<div class="m-demo__preview m-demo__preview--badge p-4">
									<p>{{ $key }}</p>
									<div class="m-checkbox-inline">
										
										@foreach($valueModulesPageData as $key => $valueModulesSlugPageData)
											@php
											$checkedData = "";
											if(in_array($valueModulesSlugPageData['page_id'], $currentData)){
												$checkedData = "checked";
											}
											@endphp
											<label class="m-checkbox">
												@if($valueModulesSlugPageData['page_slug'] == "All")
													<input {{ $checkedData }} type="checkbox" value="{{ $valueModulesSlugPageData['page_id'] }}" name="page_ids[]" onchange="checkAll({{ $valueModulesSlugPageData['page_id'] }})" id="pages{{ $valueModulesSlugPageData['page_id'] }}" class="add_class_{{ $valueModulesSlugPageData['page_id'] }}"> {{ $valueModulesSlugPageData['page_slug'] }}
													<span></span>
												@else
													<input {{ $checkedData }} type="checkbox" value="{{ $valueModulesSlugPageData['page_id'] }}" name="page_ids[]" onchange="checkSubCheckbox({{ $valueModulesSlugPageData['page_id'] }},{{ $valueModulesSlugPageData['parent_page_id'] }})" id="pages{{ $valueModulesSlugPageData['page_id'] }}" class="add_class_sub_{{ $valueModulesSlugPageData['parent_page_id'] }}"> {{ $valueModulesSlugPageData['page_slug'] }}
													<span></span>
												@endif
												
											</label>

										@endforeach
										
									</div>
								</div>
							</div> 
							@endforeach
							@if ($errors->has('page_ids'))
							<div class="text-danger">
								{{ $errors->first('page_ids') }}
							</div>
							@endif
							 
						</div>
					</div>
			</div>
			<div class="m-form__seperator m-form__seperator--dashed"></div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							{!! Form::button('Update Permission',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-primary m-btn--air','value'=>'Update Permission']) !!}
							<a href="{{ route('general-settings-role-permission') }}" class="btn btn-secondary m-btn--air">Cancel</a>
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>  
		<!--end::Portlet-->
		<!--End::Section--> 
	</div>
</div>
<!-- end:: Body -->

@endsection

@section('content_js')
<script type="text/javascript">
	function checkAll(ele) {
		var ischecked= $('.add_class_'+ele).is(':checked');
       	if(ischecked == true){
       		$('.add_class_sub_'+ele).prop('checked', true);
       	}
       	else{
       		$('.add_class_sub_'+ele).prop('checked', false);
       	}
	}

	function checkSubCheckbox(ele,parentPageId) {
		var ischecked = $('.add_class_sub_'+ele).is(':checked');
		//alert(ele);
       	if(ischecked == false){
       		$('.add_class_'+parentPageId).prop('checked', false);
       	}
	}
</script>
@endsection