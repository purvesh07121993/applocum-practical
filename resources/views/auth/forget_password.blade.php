@extends('elements.login_master')
@section('content')
<div class="">
    <div class="m-login__head">
        <h3 class="m-login__title">Forgotten Password ?</h3>
        <div class="m-login__desc">Enter your email to reset your password:</div>
    </div>
    @if (\Session::get('success'))
        <div class="alert alert-success" role="alert" id="flash_message">
            {{ \Session::get('success') }}
        </div>
        @endif
    {!! Form::open(array('route' => 'forget-password-authentication','method'=>'POST','files'=>'true','class'=>'m-login__form m-form')) !!}
        
        <div class="form-group m-form__group">
            {!! Form::email('email',null,['id'=>'email','class'=>'form-control m-input','placeholder'=>'Email']) !!}
            @if ($errors->has('email'))
                <div id="email_error" class="text-danger ml-3">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>
        <div class="m-login__form-action">
            {!! Form::button('Request',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary','value'=>'Sign In']) !!}
            
            <a href="{{ route('login-design') }}" id="m_login_forget_password" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">Cancel</a>
        </div>
    {!! Form::close() !!}
</div>
@endsection