@extends('elements.login_master')

@section('content')
<div class="m-login__signin">
    <div class="m-login__head">
        <h3 class="m-login__title">Sign In</h3>
    </div>

    <!-- @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif -->

    
    {!! Form::open(array('route' => 'login','method'=>'POST','files'=>'true','class'=>'m-login__form m-form')) !!}


        <div class="form-group m-form__group">
            {!! Form::email('email',null,['id'=>'email','class'=>'form-control m-input','placeholder'=>'Email']) !!}
            @if ($errors->has('email'))
                <div id="email_error" class="text-danger ml-3">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>

        <div class="form-group m-form__group">
            {!! Form::password('password',['id'=>'password','class'=>'form-control m-input m-login__form-input--last','placeholder'=>'Password']) !!}
            @if ($errors->has('password'))
                <div id="email_error" class="text-danger ml-3">
                    {{ $errors->first('password') }}
                </div>
            @endif
        </div>
        
        <div class="row m-login__form-sub">
            <div class="col m--align-right m-login__form-right">
                <a href="{{ route('forget-password') }}" id="m_login_forget_password" class="m-link">Forget Password ?</a>
            </div>
        </div>
        <div class="m-login__form-action">
            {!! Form::button('Sign In',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary','value'=>'Sign In']) !!}

        </div>

    {!! Form::close() !!}
  
</div>
@endsection