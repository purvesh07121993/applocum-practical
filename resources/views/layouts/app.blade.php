<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name='keywords' content='your, tags'>
	<meta name='description' content='150 words'>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="{{ URL::asset('asset/images/faviconi.png') }}" />
	<!-- <link rel="shortcut icon" type="image/png" href="images/logo.png"> -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('asset/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('asset/css/line-awesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('asset/css/font-awesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('asset/css/iziToast.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('asset/css/style.css') }}">
	<script src="{{ URL::asset('asset/js/jquery-3.3.1.min.js') }}"></script>
</head>
<body>
	<div class="bkloader" id="bkloader" style="display: none;">
        <div class="loader"></div>
    </div>
	<div class="wrapper">
		<header>
			<div class="container-fluid">
				<nav id="navigation" class="navigation clearfix">
					<div class="nav-header">
						<a class="nav-brand" href="{{ route('home') }}">
							<img src="{{ URL::asset('asset/images/logo.png') }}">
						</a>
						<div class="nav-toggle"></div>
					</div>
					<div class="nav-menus-wrapper">
						<ul class="nav-menu">
							@auth
							<li>
								<a href="#">My Account</a>
								<ul class="nav-dropdown">
									<li><a href="{{ route('user-profile') }}"><i class="la la-user"></i> Profile</a></li>
									<li><a href="{{ route('user-change-password') }}"><i class="la la-unlock"></i> Change Password</a></li>
									<li><a href="javascript:" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="la la-sign-out"></i> Logout</a></a></li>
								</ul>
							</li>
							@endauth
						</ul>
					</div>
				</nav>  
			</div>
		</header>
		@yield('content')
		<footer>
			<div class="container">
	      		<div class="copy-right">Copyright © 2019 All Rights Reserved. </div>
			</div>
		</footer>
	</div>
	<form id="frm-logout" action="{{ route('user-logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
	<script src="{{ URL::asset('asset/js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('asset/js/menu.js') }}"></script>
	<script src="{{ URL::asset('asset/js/iziToast.js') }}"></script>
	<script src="{{ URL::asset('asset/js/custom.js') }}"></script>
	<script type="text/javascript">
		setTimeout(function() { $("#flash_message").hide('slow'); }, 5000);
	</script>
	<script type="text/javascript">
		@if (\Session::get('success'))
		iziToast.success({
			title: '{{ \Session::get('success') }}',
			position: 'topRight',
			timeout: 3000,
		});
		@endif  

		@if (\Session::get('error'))
		iziToast.error({
			title: '{{ \Session::get('error') }}',
			position: 'topRight',
			timeout: 3000,
		});
		@endif
		
	</script>
	@yield('content_js')
</body>
</html>