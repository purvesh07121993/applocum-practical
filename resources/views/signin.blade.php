@extends('layouts.app')
@section('content')
<div class="section-top login-signup">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-7">
                <div class="login-signup-img text-center">
                    <img src="{{ URL::asset('asset/images/logo.png') }}">
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-5">
                <div class="white-box">
                    <div class="login-signup-form">
                        <h3>Sign In</h3>
                        {!! Form::open(array('route' => 'login','method'=>'POST','files'=>'true')) !!}
                            <div class="form-group">
                                <label class="custom-label">Email <span class="text-danger">*</span></label>
                                {!! Form::email('email',NULL,['id'=>'email','class'=>'custom-input']) !!}
                                @if ($errors->has('email'))
                                    <div class="error-message">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label class="custom-label">Password <span class="text-danger">*</span></label>
                                <input type="password" name="password" id="password" class="custom-input" value="{{ old('password') }}">
                                @if ($errors->has('password'))
                                    <div class="error-message">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>

                            {!! Form::button('Sign In',['name'=>'sbtok','type'=>'submit','id'=>'sbtok','class'=>'custom-bt block','value'=>'Sign In']) !!}
                            
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection