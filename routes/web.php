<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();
// Authentication Routes...
/*$this->get('admin', 'Auth\LoginController@showLoginForm')->name('login-design');*/
Route::get('/admin', 'HomeController@index')->name('login-design');
$this->post('admin-login', 'Auth\LoginController@login')->name('login');
$this->post('admin-logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

//Admin 
Route::get('/dashboard', 'AdminController@index')->name('admin-dashboard');


//Role pages
Route::get('/role', ['middleware' => 'role:general-settings-role','uses' =>'RoleController@index'])->name('general-settings-role');
Route::get('/role-add', ['middleware' => 'role:general-settings-role-add','uses' =>'RoleController@roleAdd'])->name('general-settings-role-add');
Route::post('/role-save', ['middleware' => 'role:general-settings-role-add','uses' =>'RoleController@saveRole'])->name('general-settings-role-save');
Route::get('/role/{id}/update', ['middleware' => 'role:general-settings-role-update','uses' =>'RoleController@roleUpdate'])->name('general-settings-role-update');
Route::post('/role-save/{id}/update', ['middleware' => 'role:general-settings-role-update','uses' =>'RoleController@saveUpdateRole'])->name('general-settings-role-save-update');
Route::get('/role/{id}/delete', ['middleware' => 'role:general-settings-role-delete','uses' =>'RoleController@deleteRole'])->name('general-settings-role-delete');


// Role Permissions
Route::get('/role-permission', ['middleware' => 'role:general-settings-role-permission','uses' =>'RolePermissionController@index'])->name('general-settings-role-permission');
Route::get('/role-permission-add', ['middleware' => 'role:general-settings-role-permission-add','uses' =>'RolePermissionController@rolePermissionAdd'])->name('general-settings-role-permission-add');
Route::post('/role-permission-save', ['middleware' => 'role:general-settings-role-permission-add','uses' =>'RolePermissionController@saveRolePermission'])->name('general-settings-role-permission-save');
Route::get('/role-permission/{id}/update', ['middleware' => 'role:general-settings-role-Permission-update','uses' =>'RolePermissionController@rolePermissionUpdate'])->name('general-settings-role-Permission-update');
Route::post('/role-permission-save/{id}/update', ['middleware' => 'role:general-settings-role-Permission-update','uses' =>'RolePermissionController@saveUpdateRolePermission'])->name('general-settings-role-permission-save-update');
Route::get('/role-permission/{id}/delete',['middleware' => 'role:general-settings-role-permission-delete','uses' => 'RolePermissionController@deletePermissionRole'])->name('general-settings-role-permission-delete');


//Admin Profile & change password
Route::get('/admin-profile', 'AdminController@profileUpdate')->name('admin-profile');
Route::post('/admin-profile-save/{id}/update', 'AdminController@profileUpdateSave')->name('admin-profile-update');
Route::get('/change-password', 'AdminController@changePassword')->name('change-password');
Route::post('/change-password-save', 'AdminController@changePasswordSave')->name('change-password-save');


// Manage Customer
Route::get('/customer-agent', ['middleware' => 'role:customer-agent','uses' =>'CustomerController@index'])->name('customer-agent');
Route::get('/customer-agent-add', ['middleware' => 'role:customer-add','uses' =>'CustomerController@customerAdd'])->name('customer-add');
Route::post('/customer-agent-save', ['middleware' => 'role:customer-add','uses' =>'CustomerController@saveCustomer'])->name('customer-save');
Route::get('/customer-agent/{id}/update', ['middleware' => 'role:customer-update','uses' =>'CustomerController@customerUpdate'])->name('customer-update');
Route::post('/customer-agent-save/{id}/update', ['middleware' => 'role:customer-update','uses' =>'CustomerController@saveUpdateCustomer'])->name('customer-save-update');
Route::get('/customer-agent/{id}/delete', ['middleware' => 'role:customer-delete','uses' =>'CustomerController@deleteCustomer'])->name('customer-delete');
Route::get('/customer-agent/{id}/details',['middleware' => 'role:customer-details','uses' => 'CustomerController@customerDetailsView'])->name('customer-details');


// Manage Admin
Route::get('/admin-user', ['middleware' => 'role:admin-user','uses' => 'AdminController@adminList'])->name('admin-user');
Route::get('/admin-user-add', ['middleware' => 'role:admin-add','uses' =>'AdminController@adminAdd'])->name('admin-add');
Route::post('/admin-user-save', 'AdminController@saveAdmin')->name('admin-save');
Route::get('/admin-user/{id}/update', ['middleware' => 'role:admin-update','uses' =>'AdminController@adminUpdate'])->name('admin-update');
Route::post('/admin-user-save/{id}/update', 'AdminController@saveUpdateAdmin')->name('admin-save-update');
Route::get('/admin-user/{id}/delete',['middleware' => 'role:admin-delete','uses' => 'AdminController@deleteAdmin'])->name('admin-delete');
Route::get('/admin-user/{id}/details',['middleware' => 'role:admin-delete','uses' => 'AdminController@adminDetailsView'])->name('admin-details');




//Other pages
Route::get('/', 'HomeController@index')->name('home');
Route::get('/Sign-In', 'HomeController@signIn')->name('sign-in');
$this->post('user-logout', 'HomeController@logout')->name('user-logout');
